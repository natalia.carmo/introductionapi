<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Playlist;
use Illuminate\Support\Facades\Validator;

class PlaylistController extends Controller
{
    public function store(Request $request){
        try{
            $validation = Validator::make($request->all(), [
                'name'=> 'required|max:255',
                'is_public'=> 'required|boolean'
            ], [
                'required'=> 'O campo :attribute é obrigatório',
                'max' => 'O campo :attribute é obrigatório',
                'boolean'=> 'Verdadeiro ou falso'
                
            ]);
    
            if($validation->fails()){
                return response()->json([
                    'status_message'=> 'ocorreu um erro',
                    'error_message'=> $validation->errors()
                ], 400);
            }
    
    
            $playlist = Playlist::create($request->only('name','is_public'));
            
            $response = [
                'status_message'=> 'criado com sucesso',
                'id'=> $playlist->id 
            ];
    
            return response()->json($response, 201);
           }
        
           
           catch(\Excption $e){
               return response()->json([
                   'status_message'=> 'erro no servidor',
                   'error_message'=> $e->getMessage()
               ], 500);
           }
        
    }
}

