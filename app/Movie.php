<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model 
{
    protected $fillable = ["name", "description", "year"];

    protected $dates = ['Y-m-d'];

    protected $hidden = ['created_at', 'updated_at'];
}