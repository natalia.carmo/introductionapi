<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use Illuminate\Support\Facades\Validator;

class MovieController extends Controller
{   
    public function store(Request $request){

       try{
        $validation = Validator::make($request->all(), [
            'name'=> 'required|max:255',
            'description'=> 'required|max:2000',
            'year'=> 'required|date'
        ], [
            'required'=> 'O campo :attribute é obrigatório',
            'max' => 'O campo :attribute é obrigatório',
            'date' => 'O campo :attribute é obrigatório'
        ]);

        if($validation->fails()){
            return response()->json([
                'status_message'=> 'ocorreu um erro',
                'error_message'=> $validation->errors()
            ], 400);
        }


        $movie = Movie::create($request->only('name', 'description', 'year'));
        
        $response = [
            'status_message'=> 'criado com sucesso',
            'id'=> $movie->id 
        ];

        return response()->json($response, 201);
       }
    
       
       catch(\Excption $e){
           return response()->json([
               'status_message'=> 'erro no servidor',
               'error_message'=> $e->getMessage()
           ], 500);
       }
    }

    public function get($movie_id){
        $movie = Movie::find($movie_id);
        if(empty($movie)){
            return response()->json([
                'status_message'=> 'ocorreu um erro',
                'error_message' => 'Movie not found'
            ], 404);
        }

        return response()->json([
            'status_message'=> 'movie encotrado',
            'movie'=>$movie
        ], 200);
    
    }

    public function update(Request $request, $movie_id){
        $movie = Movie::find($movie_id);
        if(empty($movie)){
            return response()->json([
                'status_message'=> 'ocorreu um erro',
                'error_message' => 'Movie not found'
            ], 404);
        }

        $validation = Validator::make($request->all(), [
            'name'=> 'max:255',
            'description'=> 'max:2000',
            'year'=> 'date'
        ], [
            'required'=> 'O campo :attribute é obrigatório',
            'max' => 'O campo :attribute é obrigatório',
            'date' => 'O campo :attribute é obrigatório'
        ]);

        if($validation->fails()){
            return response()->json([
                'status_message'=> 'ocorreu um erro',
                'error_message'=> $validation->errors()
            ], 400);
        }

        $movie->fill($request->only('name', 'description', 'year'));
        $movie->save();

        return response()->json([
            'status_message'=> 'atualizado com sucesso',
            'movie'=> $movie
        ], 201);
    }
    
}
